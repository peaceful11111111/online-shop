const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const babel = require('gulp-babel');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const clean = require('gulp-clean');
const imageMin = require('gulp-imagemin');
const jsMinify = require('gulp-js-minify');
const uglify = require('gulp-uglify');
const cleanCSS = require('gulp-clean-css');

const path = {
    dist: {
        html: 'dist',
        css: 'dist/css',
        js: 'dist/js',
        img: 'dist/img',
        self: 'dist',
    },
    src: {
        html: 'src/*.html',
        scss: 'src/scss/*.scss',
        js: 'src/js/*.js',
        img: 'src/img/**/*.*'
    }
};

const imgBuild = () => (
    gulp.src(path.src.img)
        .pipe(imageMin([
                imageMin.gifsicle({interlaced: true}),
                imageMin.jpegtran({progressive: true}),
                imageMin.optipng({optimizationLevel: 5}),
                imageMin.svgo({
                    plugins: [
                        {removeViewBox: true},
                        {cleanupIDs: false}
                    ]
                })
            ]))
        .pipe(gulp.dest(path.dist.img))
        .pipe(browserSync.stream())
);

const htmlBuild = () => (
    gulp.src(path.src.html)
        .pipe(gulp.dest(path.dist.html))
        .pipe(browserSync.stream())
);

const scssBuild = () => (
    gulp.src(path.src.scss)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer(['> 0.01%', 'last 100 versions']))
        .pipe(gulp.dest(path.dist.css))
        .pipe(browserSync.stream())
);

const jsBuild = () => (
  gulp.src(path.src.js)
      .pipe(concat('script.js'))
      .pipe(babel({
          presets: ['@babel/env']
      }))
      // .pipe(jsMinify())
      // .pipe(uglify())
      .pipe(gulp.dest(path.dist.js))
      .pipe(browserSync.stream())
);

const cleanDist = () => (
  gulp.src(path.dist.self, {allowEmpty: true})
      .pipe(clean())
);


const cleanCss = () => (
     gulp.src(path.dist.css, {allowEmpty: true})
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(path.dist.self))
);

const watcher = () => {
    browserSync.init({
        server: {
            baseDir: './dist'
        }
    });

    gulp.watch(path.src.html, htmlBuild).on('change', browserSync.reload);
    gulp.watch(path.src.scss, scssBuild).on('change', browserSync.reload);
    gulp.watch(path.src.js, jsBuild).on('change', browserSync.reload);
    gulp.watch(path.src.img, imgBuild).on('change', browserSync.reload);
};

gulp.task('html', htmlBuild);
gulp.task('scss', scssBuild);
gulp.task('js', jsBuild);
gulp.task('img', imgBuild);
gulp.task('clean-css', cleanCss);

gulp.task('default', gulp.series(
    cleanDist,
    cleanCss,
    gulp.parallel(imgBuild, htmlBuild, scssBuild, jsBuild),
    watcher
));

