const input = document.querySelector('.top-menu-checkbox');
const burgerBg = document.querySelector('.top-menu-burger-bg');

input.addEventListener('click', (event) => {
  console.log(event.target);
  if (event.target === input) {
    burgerBg.classList.toggle('active-burger-bg')
  }
});


// ********* HOVER ON FURNITURE CARD ********* //


const furnitureCardsImages = document.querySelectorAll('.furniture-card-img-wrapper');
furnitureCardsImages.forEach((el) => {
  el.addEventListener('mouseover', highlightCardElements);
  el.addEventListener('mouseleave', removeHighlightCardElements)
});

function highlightCardElements(evt) {
  const furnitureCard = evt.currentTarget.parentElement;
  if (!evt.currentTarget.classList.contains('furniture-card-img-wrapper')) {
    return;
  }
  furnitureCard.classList.add('furniture-card-active');
}

function removeHighlightCardElements(evt) {
  const furnitureCard = evt.currentTarget.parentElement;
  furnitureCard.classList.remove('furniture-card-active');
}


// ********* PRICE FILTER SLIDER ********* //

document.addEventListener('mousedown', (e) => {
  if (e.target.classList.contains('ui-slider-handle')) {
    document.addEventListener('mousemove', showPriceRange);
    document.addEventListener('mouseup', () => {
      document.removeEventListener('mousemove', showPriceRange)
    })
  }
});

function showPriceRange(e) {

  let minPrice = document.querySelector('.price-range-min').innerHTML;
  let maxPrice = document.querySelector('.price-range-max').innerHTML;

  if (isNaN(minPrice.substring(1, 3))) {
    return;
  }

  // ЕСЛИ ЦЕНОВОЙ СЛАЙДЕР БУДЕТ ОДИН
  // document.querySelector('.furniture-filter-price-minPrice').innerHTML = `${minPrice}`;
  // if (minPrice === maxPrice) {
  //   document.querySelector('.furniture-filter-price-maxPrice').innerHTML = "";
  // } else {
  //   document.querySelector('.furniture-filter-price-maxPrice').innerHTML = `-${maxPrice}`;
  // }

  const minPriceSpans = document.querySelectorAll('.furniture-filter-price-minPrice');
  const maxPriceSpans = document.querySelectorAll('.furniture-filter-price-maxPrice');

  minPriceSpans.forEach(el => {
    el.innerHTML = `${minPrice}`;
  });

  maxPriceSpans.forEach(el => {
    minPrice === maxPrice ? el.innerHTML = "": el.innerHTML = `-${maxPrice}`;
  });
}

// ******** HIGHLIGHT / UNHIGHLIGHT SLIDER BUTTON (NOT DEPENDING WHERE MOUSEUP HAPPENED) ********* //
let priceRangeBtns = document.querySelectorAll('.ui-slider-handle');
priceRangeBtns.forEach(el => {
  el.addEventListener('mousedown', (e) => {
    e.currentTarget.classList.add('chosen');
    document.addEventListener('mouseup', (e) => {
      priceRangeBtns.forEach(el => {
        el.classList.contains('chosen') ? el.classList.remove('chosen') : "";
      });
    });
  });
});


// ******** HIGHLIGHT / UNHIGHLIGHT TAG FILTER ITEMS ********* //
let filterByTagItems = document.querySelectorAll('.furniture-filter-tag-item');
filterByTagItems.forEach(el => {
  el.addEventListener('click', highlightChosenItem);
});

function highlightChosenItem(e) {
  e.currentTarget.classList.toggle('chosen-tag-item')
}


// ************ SPINNER ANIMATION ********** //

const spinner = document.querySelector('.furniture-spinner');
spinner.addEventListener('click', spin);

function spin(e) {
  const spinnedImg = e.currentTarget.firstElementChild;
  spinnedImg.classList.add('furniture-spinner-img-animation');
  setTimeout(() => {
    spinnedImg.parentElement.remove();
    showMoreProductCards();
  }, 2000);
}

function showMoreProductCards() {
  for (let i = 1; i <= 9; i++) {
    let furnitureCard = document.querySelector(`.furniture-card:nth-child(${9 + i})`)
    furnitureCard.classList.remove('d-none');
  }
}


// ************ BASKET COUNTER ********** //


const basketCounterEl = document.querySelector('.top-menu-icon-container-counter');
let basketCounter = 0;
let addToBasketBtns = document.querySelectorAll('.furniture-card-btn-basket');

addToBasketBtns.forEach(el => {
  el.addEventListener('click', countItemsInBasket)
});

function countItemsInBasket(e) {
  basketCounter += 1;
  basketCounterEl.innerText = `${basketCounter}`
}
